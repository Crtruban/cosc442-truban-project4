package edu.towson.cis.cosc442.project4.coffeemaker;

import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>CoffeeMakerTest</code> contains tests for the class <code>{@link CoffeeMaker}</code>.
 *
 * @generatedBy CodePro at 3/6/21, 2:30 PM
 * @author Camryn
 * @version $Revision: 1.0 $
 */
public class CoffeeMakerTest {
	/**
	 * Run the CoffeeMaker() constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testCoffeeMaker_1()
		throws Exception {

		CoffeeMaker result = new CoffeeMaker();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the CoffeeMaker() constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testCoffeeMaker_2()
		throws Exception {

		CoffeeMaker result = new CoffeeMaker();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the boolean addInventory(int,int,int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testAddInventory_1()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();


		boolean result = fixture.addInventory(0, 1, 1, 1);

		// add additional test code here
		assertEquals(true, result);
		assertEquals(fixture.checkInventory().getCoffee(),15);
		assertEquals(fixture.checkInventory().getMilk(),16);
		assertEquals(fixture.checkInventory().getSugar(),16);
		assertEquals(fixture.checkInventory().getChocolate(),16);
	}

	/**
	 * Run the boolean addInventory(int,int,int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testAddInventory_2()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		int amtCoffee = 1;
		int amtMilk = 1;
		int amtSugar = 0;
		int amtChocolate = 1;

		boolean result = fixture.addInventory(1, 0, 1, 1);

		// add additional test code here
		assertEquals(true, result);
		assertEquals(fixture.checkInventory().getCoffee(),16);
		assertEquals(fixture.checkInventory().getMilk(),15);
		assertEquals(fixture.checkInventory().getSugar(),16);
		assertEquals(fixture.checkInventory().getChocolate(),16);
	}

	/**
	 * Run the boolean addInventory(int,int,int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testAddInventory_3()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		int amtCoffee = -1;
		int amtMilk = 1;
		int amtSugar = 1;
		int amtChocolate = 1;

		boolean result = fixture.addInventory(1, 1, 0, 1);

		// add additional test code here
		assertEquals(true, result);
		assertEquals(fixture.checkInventory().getCoffee(),16);
		assertEquals(fixture.checkInventory().getMilk(),16);
		assertEquals(fixture.checkInventory().getSugar(),15);
		assertEquals(fixture.checkInventory().getChocolate(),16);
	}

	/**
	 * Run the boolean addInventory(int,int,int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testAddInventory_4()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		int amtCoffee = 1;
		int amtMilk = -1;
		int amtSugar = 1;
		int amtChocolate = 1;

		boolean result = fixture.addInventory(1, 1, 1, 0);

		// add additional test code here
		assertEquals(true, result);
		assertEquals(fixture.checkInventory().getCoffee(),16);
		assertEquals(fixture.checkInventory().getMilk(),16);
		assertEquals(fixture.checkInventory().getSugar(),16);
		assertEquals(fixture.checkInventory().getChocolate(),15);
	}

	/**
	 * Run the boolean addInventory(int,int,int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testAddInventory_5()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();

		
		boolean result = fixture.addInventory(-2, 1, 3, 2);

		// add additional test code here
		assertEquals(false, result);
		assertEquals(fixture.checkInventory().getCoffee(),15);
		assertEquals(fixture.checkInventory().getMilk(),15);
		assertEquals(fixture.checkInventory().getSugar(),15);
		assertEquals(fixture.checkInventory().getChocolate(),15);
	}
	@Test
	public void testAddInventory_6()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		int amtCoffee = -1;
		int amtMilk = 1;
		int amtSugar = 1;
		int amtChocolate = 1;
		
		boolean result = fixture.addInventory(1, -3, 2, 2);

		// add additional test code here
		assertEquals(false, result);
		assertEquals(fixture.checkInventory().getCoffee(),15);
		assertEquals(fixture.checkInventory().getMilk(),15);
		assertEquals(fixture.checkInventory().getSugar(),15);
		assertEquals(fixture.checkInventory().getChocolate(),15);
	}
	@Test
	public void testAddInventory_7()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		int amtCoffee = -1;
		int amtMilk = 1;
		int amtSugar = 1;
		int amtChocolate = 1;
		
		boolean result = fixture.addInventory(1, 1, -3, 1);

		// add additional test code here
		assertEquals(false, result);
		assertEquals(fixture.checkInventory().getCoffee(),15);
		assertEquals(fixture.checkInventory().getMilk(),15);
		assertEquals(fixture.checkInventory().getSugar(),15);
		assertEquals(fixture.checkInventory().getChocolate(),15);
	}
	@Test
	public void testAddInventory_8()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		int amtCoffee = -1;
		int amtMilk = 1;
		int amtSugar = 1;
		int amtChocolate = 1;
		
		boolean result = fixture.addInventory(1, 1, 1, -2);

		// add additional test code here
		assertEquals(false, result);
		assertEquals(fixture.checkInventory().getCoffee(),15);
		assertEquals(fixture.checkInventory().getMilk(),15);
		assertEquals(fixture.checkInventory().getSugar(),15);
		assertEquals(fixture.checkInventory().getChocolate(),15);
	}

	/**
	 * Run the boolean addRecipe(Recipe) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testAddRecipe_1()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		Recipe r = new Recipe();

		boolean result = fixture.addRecipe(r);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean addRecipe(Recipe) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testAddRecipe_2()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		Recipe r = new Recipe();

		boolean result = fixture.addRecipe(r);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean addRecipe(Recipe) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testAddRecipe_3()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		Recipe r = new Recipe();

		boolean result = fixture.addRecipe(r);

		// add additional test code here
		assertEquals(true, result);
	}
	@Test
	public void testAddRecipe_4()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		Recipe r = new Recipe();
		Recipe e = new Recipe();
		Recipe d = new Recipe();
		Recipe c = new Recipe();
		Recipe a = new Recipe();
		fixture.addRecipe(r);
		fixture.addRecipe(a);
		fixture.addRecipe(d);
		fixture.addRecipe(c);
		boolean result = fixture.addRecipe(e);

		// add additional test code here
		assertEquals(false, result);
	}
	@Test
	public void testAddRecipe_5()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		Recipe r = new Recipe();
		Recipe a = new Recipe();
		Recipe d = new Recipe();
		fixture.addRecipe(r);
		fixture.addRecipe(a);
		fixture.addRecipe(d);
		boolean result = fixture.addRecipe(r);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the Inventory checkInventory() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testCheckInventory_1()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();

		Inventory result = fixture.checkInventory();

		// add additional test code here
		assertNotNull(result);
		assertEquals("Coffee: 15\r\nMilk: 15\r\nSugar: 15\r\nChocolate: 15\r\n", result.toString());
		assertEquals(15, result.getChocolate());
		assertEquals(15, result.getCoffee());
		assertEquals(15, result.getMilk());
		assertEquals(15, result.getSugar());
	}

	/**
	 * Run the boolean deleteRecipe(Recipe) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testDeleteRecipe_1()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		Recipe r = new Recipe();
		r.setName("CAS");
		fixture.addRecipe(r);

		boolean result = fixture.deleteRecipe(r);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean deleteRecipe(Recipe) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testDeleteRecipe_2()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		Recipe r = new Recipe();

		boolean result = fixture.deleteRecipe(r);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean deleteRecipe(Recipe) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testDeleteRecipe_3()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		Recipe r = new Recipe();

		boolean result = fixture.deleteRecipe(r);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean deleteRecipe(Recipe) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testDeleteRecipe_4()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		Recipe r = null;

		boolean result = fixture.deleteRecipe(r);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean editRecipe(Recipe,Recipe) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testEditRecipe_1()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		Recipe oldRecipe = new Recipe();
		Recipe newRecipe = new Recipe();

		boolean result = fixture.editRecipe(oldRecipe, newRecipe);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean editRecipe(Recipe,Recipe) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testEditRecipe_2()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		Recipe oldRecipe = new Recipe();
		Recipe newRecipe = new Recipe();

		boolean result = fixture.editRecipe(oldRecipe, newRecipe);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean editRecipe(Recipe,Recipe) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testEditRecipe_3()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		Recipe oldRecipe = new Recipe();
		Recipe newRecipe = new Recipe();
		
		boolean result = fixture.editRecipe(oldRecipe, newRecipe);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean editRecipe(Recipe,Recipe) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testEditRecipe_4()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		Recipe oldRecipe = new Recipe();
		Recipe newRecipe = new Recipe();

		boolean result = fixture.editRecipe(oldRecipe, newRecipe);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean editRecipe(Recipe,Recipe) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testEditRecipe_5()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		Recipe oldRecipe = new Recipe();
		Recipe newRecipe = new Recipe();

		boolean result = fixture.editRecipe(oldRecipe, newRecipe);

		// add additional test code here
		assertEquals(false, result);
	}
	@Test
	public void testEditRecipe_6()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		Recipe oldRecipe = new Recipe();
		Recipe newRecipe = new Recipe();
		newRecipe.setName("New");
		oldRecipe.setName("Old");
		fixture.addRecipe(oldRecipe);

		

		boolean result = fixture.editRecipe(oldRecipe, newRecipe);

		// add additional test code here
		assertEquals(true, result);
	}

	@Test
	public void testAddRecipe_8() throws Exception
	{
		CoffeeMaker norton = new CoffeeMaker();
		Recipe oldRecipe = new Recipe();
		oldRecipe.setName("Test Name");
		norton.addRecipe(oldRecipe);
		assertEquals(false,norton.addRecipe(oldRecipe));
	}


	/**
	 * Run the Recipe getRecipeForName(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testGetRecipeForName_1()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		String name = "";

		Recipe result = fixture.getRecipeForName(name);

		// add additional test code here
		assertNotNull(result);
		assertEquals(null, result.getName());
		assertEquals(null, result.toString());
		assertEquals(0, result.getAmtCoffee());
		assertEquals(0, result.getAmtMilk());
		assertEquals(0, result.getPrice());
		assertEquals(0, result.getAmtSugar());
		assertEquals(0, result.getAmtChocolate());
	}
	
	/**
	 * Running the Recipe getRecipeForName(String) method test.
	 * 
	 * @throws Exception
	 * 
	 * Aiming for non-null results.
	 */
	@Test
	public void testGetRecipeForName_NN() throws Exception
	{
		CoffeeMaker tasty = new CoffeeMaker();
		String name = "NonNull";
		
		Recipe cafe = new Recipe();
		cafe.setName(name);
		cafe.setAmtMilk(1);
		cafe.setAmtCoffee(1);
		cafe.setAmtSugar(1);
		cafe.setAmtChocolate(0);
		tasty.addRecipe(cafe);
		assertEquals(cafe,tasty.getRecipeForName(name));
	}

	/**
	 * Run the Recipe getRecipeForName(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testGetRecipeForName_2()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		String name = "";

		Recipe result = fixture.getRecipeForName(name);

		// add additional test code here
		assertNotNull(result);
		assertEquals(null, result.getName());
		assertEquals(null, result.toString());
		assertEquals(0, result.getAmtCoffee());
		assertEquals(0, result.getAmtMilk());
		assertEquals(0, result.getPrice());
		assertEquals(0, result.getAmtSugar());
		assertEquals(0, result.getAmtChocolate());
	}

	/**
	 * Run the Recipe getRecipeForName(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testGetRecipeForName_3()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		String name = "";

		Recipe result = fixture.getRecipeForName(name);

		// add additional test code here
		assertNotNull(result);
		assertEquals(null, result.getName());
		assertEquals(null, result.toString());
		assertEquals(0, result.getAmtCoffee());
		assertEquals(0, result.getAmtMilk());
		assertEquals(0, result.getPrice());
		assertEquals(0, result.getAmtSugar());
		assertEquals(0, result.getAmtChocolate());
	}

	/**
	 * Run the Recipe getRecipeForName(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testGetRecipeForName_4()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		String name = "";

		Recipe result = fixture.getRecipeForName(name);

		// add additional test code here
		assertNotNull(result);
		assertEquals(null, result.getName());
		assertEquals(null, result.toString());
		assertEquals(0, result.getAmtCoffee());
		assertEquals(0, result.getAmtMilk());
		assertEquals(0, result.getPrice());
		assertEquals(0, result.getAmtSugar());
		assertEquals(0, result.getAmtChocolate());
	}

	/**
	 * Run the Recipe[] getRecipes() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testGetRecipes_1()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();

		Recipe[] result = fixture.getRecipes();

		// add additional test code here
		assertNotNull(result);
		assertEquals(4, result.length);
		assertNotNull(result[0]);
		assertEquals(null, result[0].getName());
		assertEquals(null, result[0].toString());
		assertEquals(0, result[0].getAmtCoffee());
		assertEquals(0, result[0].getAmtMilk());
		assertEquals(0, result[0].getPrice());
		assertEquals(0, result[0].getAmtSugar());
		assertEquals(0, result[0].getAmtChocolate());
		assertNotNull(result[1]);
		assertEquals(null, result[1].getName());
		assertEquals(null, result[1].toString());
		assertEquals(0, result[1].getAmtCoffee());
		assertEquals(0, result[1].getAmtMilk());
		assertEquals(0, result[1].getPrice());
		assertEquals(0, result[1].getAmtSugar());
		assertEquals(0, result[1].getAmtChocolate());
		assertNotNull(result[2]);
		assertEquals(null, result[2].getName());
		assertEquals(null, result[2].toString());
		assertEquals(0, result[2].getAmtCoffee());
		assertEquals(0, result[2].getAmtMilk());
		assertEquals(0, result[2].getPrice());
		assertEquals(0, result[2].getAmtSugar());
		assertEquals(0, result[2].getAmtChocolate());
		assertNotNull(result[3]);
		assertEquals(null, result[3].getName());
		assertEquals(null, result[3].toString());
		assertEquals(0, result[3].getAmtCoffee());
		assertEquals(0, result[3].getAmtMilk());
		assertEquals(0, result[3].getPrice());
		assertEquals(0, result[3].getAmtSugar());
		assertEquals(0, result[3].getAmtChocolate());

	}

	/**
	 * Run the int makeCoffee(Recipe,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testMakeCoffee_1()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		Recipe r = new Recipe();
		int amtPaid = 1;

		int result = fixture.makeCoffee(r, amtPaid);

		// add additional test code here
		assertEquals(1, result);
	}

	/**
	 * Run the int makeCoffee(Recipe,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Test
	public void testMakeCoffee_2()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		Recipe r = new Recipe();
		int amtPaid = 4;
		r.setPrice(2);
		r.setAmtCoffee(2);
		r.setAmtMilk(4);
		r.setAmtSugar(3);
		r.setAmtChocolate(1);

		int result = fixture.makeCoffee(r, amtPaid);

		// add additional test code here
		assertEquals(2, result);
		assertEquals(fixture.checkInventory().getCoffee(),13);
		assertEquals(fixture.checkInventory().getMilk(),11);
		assertEquals(fixture.checkInventory().getSugar(),12);
		assertEquals(fixture.checkInventory().getChocolate(),14);
	}
	@Test
	public void testMakeCoffee_3()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		Recipe r = new Recipe();
		int amtPaid = -1;

		int result = fixture.makeCoffee(r, amtPaid);

		// add additional test code here
		assertEquals(-1, result);
	}
	@Test
	public void testMakeCoffee_4()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		Recipe r = new Recipe();
		r.setPrice(3);
		int amtPaid = 1;

		int result = fixture.makeCoffee(r, amtPaid);

		// add additional test code here
		assertEquals(1, result);
	}
	@Test
	public void testMakeCoffee_5()
		throws Exception {
		CoffeeMaker fixture = new CoffeeMaker();
		Recipe r = new Recipe();
		r.setPrice(3);
		r.setAmtMilk(16);
		int amtPaid = 1;

		int result = fixture.makeCoffee(r, amtPaid);

		// add additional test code here
		assertEquals(1, result);
	}
	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 3/6/21, 2:30 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(CoffeeMakerTest.class);
	}
}
