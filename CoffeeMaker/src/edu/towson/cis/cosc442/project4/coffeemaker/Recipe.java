package edu.towson.cis.cosc442.project4.coffeemaker;

/**
 * Recipe object for the coffee maker
 * @author Josh
 * @version $Revision: 1.0 $
 */
public class Recipe {
    private String name;
    private int price;
    private int amtCoffee;
    private int amtMilk;
    private int amtSugar;
    private int amtChocolate;
    
    /**
     * Method getAmtChocolate.
     * @return int
     */
    public int getAmtChocolate() {
        return amtChocolate;
    }
    /**
     * Method setAmtChocolate.
     * @param amtChocolate int
     */
    public void setAmtChocolate(int amtChocolate) {
    	if(amtChocolate < 1) {
    		this.amtChocolate = 0;
    	}
    	else {
    		this.amtChocolate = amtChocolate;
    		
    	}
    }
    /**
     * Method getAmtCoffee.
     * @return int
     */
    public int getAmtCoffee() {
        return amtCoffee;
    }
    /**
     * Method setAmtCoffee.
     * @param amtCoffee int
     */
    public void setAmtCoffee(int amtCoffee) {
    	if(amtCoffee < 1) {
    		this.amtCoffee = 0;
    		
    	}
    	else {
    		this.amtCoffee = amtCoffee;
    	}
    }
    /**
     * Method getAmtMilk.
     * @return int
     */
    public int getAmtMilk() {
        return amtMilk;
    }
    /**
     * Method setAmtMilk.
     * @param amtMilk int
     */
    public void setAmtMilk(int amtMilk) {
    	if(amtMilk < 1) {
    		this.amtMilk = 0;
    		
    	}
    	else {
    		this.amtMilk = amtMilk;
    	}
    }
    /**
     * Method getAmtSugar.
     * @return int
     */
    public int getAmtSugar() {
        return amtSugar;
    }
    /**
     * Method setAmtSugar.
     * @param amtSugar int
     */
    public void setAmtSugar(int amtSugar) {
    	if(amtSugar < 1) {
    		
    		this.amtSugar = 0;
    	}
    	else {
    		this.amtSugar = amtSugar;
    	}
    }
    /**
     * Method getName.
     * @return String
     */
    public String getName() {
        return name;
    }
    /**
     * Method setName.
     * @param name String
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * Method getPrice.
     * @return int
     */
    public int getPrice() {
        return price;
    }
    /**
     * Method setPrice.
     * @param price int
     */
    public void setPrice(int price) {
    	if(price < 1) {
    		this.price = 0;
    		
    	}
    	else {
    		this.price = price;
    	}
    
    } 
    /**
     * Method equals.
     * @param r Recipe
     * @return boolean
     */
    public boolean equals(Recipe r) {
        if(this.name == null) {
        	return false;
        }
        if(r.getName() == null) {
	    	return false;
    	}	
        if((this.name).equals(r.getName())) {
            return true;
        }
        else
        return false;
    }
    /**
     * Method toString.
     * @return String
     */
    public String toString() {
    	return name;
    }
}
